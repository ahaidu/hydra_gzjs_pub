Hydra Gazebo Node-Js Publisher
=========


Gazebo hydra publisher on the `~/hydra` topic


Installation
--------------
```
$ npm install hydra_gzjs_pub

```

**Or**

Clone the git repository and in the `hydra_gzjs_pub` folder:

```
$ npm install gazebojs node-sixense

```

Usage:
--------------
start gzserver:
```
$ gzserver
```

start the publisher:
```
$ [sudo] node hydra_pub.js
```

Troubleshooting
--------------
The position and orientation axis differ and are offseted from other hydra publishers, please change accordingly in the source code (`hydra_pub.js`).

